package org.mixteco.nyt.model

import com.google.gson.annotations.SerializedName

class BookResponse(
        @SerializedName("results") val results: BookResult,
        @SerializedName("status") val status: String,
        @SerializedName("num_results") val numResults: String,
        @SerializedName("copyright") val copyRight: String
)