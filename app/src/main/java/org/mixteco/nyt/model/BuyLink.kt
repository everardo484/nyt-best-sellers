package org.mixteco.nyt.model

import com.google.gson.annotations.SerializedName

class BuyLink(@SerializedName("name") val name: String, @SerializedName("url") val url: String)