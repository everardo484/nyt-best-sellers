package org.mixteco.nyt.model

import com.google.gson.annotations.SerializedName

class BookList(
        @SerializedName("books") val books: MutableList<Book>,
        @SerializedName("list_image_width") val listImgWidth: String,
        @SerializedName("display_name") val displayName: String,
        @SerializedName("list_image_height") val listImgHeight: String,
        @SerializedName("updated") val updated: String,
        @SerializedName("list_name_encoded") val listNameEncoded: String,
        @SerializedName("list_name") val listName: String,
        @SerializedName("list_image") val listImg: String,
        @SerializedName("list_id") val listId: String
)