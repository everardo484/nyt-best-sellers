package org.mixteco.nyt.model

import com.google.gson.annotations.SerializedName

class Book(
        @SerializedName("created_date") val createdDate: String,
        @SerializedName("article_chapter_link") val articleChapterLink: String,
        @SerializedName("book_image_height") val bookImgHeight: String,
        @SerializedName("sunday_review_link") val sundayReviewLink: String,
        @SerializedName("first_chapter_link") val firstChapterLink: String,
        @SerializedName("primary_isbn10") val primaryIsbn10: String,
        @SerializedName("book_review_link") val bookReviewLink: String,
        @SerializedName("primary_isbn13") val primaryIsbn13: String,
        @SerializedName("book_image_width") val bookImgWidth: String,
        @SerializedName("publisher") val publisher: String,
        @SerializedName("weeks_on_list") val weeksOnList: String,
        @SerializedName("author") val author: String,
        @SerializedName("book_image") val bookImg: String,
        @SerializedName("title") val title: String,
        @SerializedName("rank") val rank: String,
        @SerializedName("updated_date") val updatedDate: String,
        @SerializedName("price") val price: String,
        @SerializedName("contributor") val contributor: String,
        @SerializedName("description") val description: String,
        @SerializedName("age_group") val ageGroup: String,
        @SerializedName("contributor_note") val contributorNote: String,
        @SerializedName("rank_last_week") val rankLastWeek: String,
        @SerializedName("amazon_product_url") val amazonProductUrl: String,
        @SerializedName("buy_links") val buyLinks: List<BuyLink>
)