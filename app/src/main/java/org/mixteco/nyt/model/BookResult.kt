package org.mixteco.nyt.model

import com.google.gson.annotations.SerializedName

class BookResult(
        @SerializedName("published_date") val publishedDate: String,
        @SerializedName("previous_published_date") val prevPublishedDate: String,
        @SerializedName("next_published_date") val nextPublishedDate: String,
        @SerializedName("lists") val bookList: List<BookList>,
        @SerializedName("bestsellers_date") val bestsellersData: String

)