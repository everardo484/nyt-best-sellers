package org.mixteco.nyt.di.qualifier

import javax.inject.Qualifier

@Qualifier
annotation class ApplicationQualifier