package org.mixteco.nyt.di.subcomponent.main

import dagger.Module
import dagger.Provides
import org.mixteco.nyt.data.DataSource
import org.mixteco.nyt.di.ActivityModule
import org.mixteco.nyt.di.scope.ActivityScope
import org.mixteco.nyt.model.Book
import org.mixteco.nyt.ui.MainPresenter
import org.mixteco.nyt.ui.MainActivity
import org.mixteco.nyt.ui.MainView

@Module
class MainModule(activity: MainActivity) : ActivityModule(activity) {

    @Provides
    @ActivityScope
    fun provideMainView(): MainView = activity as MainView

    @Provides
    @ActivityScope
    fun provideMainPresenter(view: MainView, dataSource: DataSource) = MainPresenter(view, dataSource)

    @Provides
    @ActivityScope
    fun provideBookDataSet(): MutableList<Book> = ArrayList()
}