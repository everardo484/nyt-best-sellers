package org.mixteco.nyt.di

import android.content.Context
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import org.mixteco.nyt.R
import org.mixteco.nyt.data.BooksRequestInterceptor
import org.mixteco.nyt.data.DataSource
import org.mixteco.nyt.di.qualifier.ApiKey
import org.mixteco.nyt.di.qualifier.ApplicationQualifier
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @Singleton
    fun provideCache(@ApplicationQualifier context: Context) = Cache(context.cacheDir, 10 * 1024 * 1024)

    @Provides
    @Singleton
    @ApiKey
    fun provideApiKey(@ApplicationQualifier context: Context): String = context.getString(R.string.apiKey)

    @Provides
    @Singleton
    fun provideOkHttpClient(cache: Cache, interceptor: BooksRequestInterceptor):
            OkHttpClient = OkHttpClient().newBuilder().cache(cache).addInterceptor(interceptor).build()

    @Provides
    @Singleton
    fun provideRequestInterceptor(@ApiKey apiKey: String):
            BooksRequestInterceptor = BooksRequestInterceptor(apiKey)

    @Provides
    @Singleton
    fun provideRetrofit(client: OkHttpClient, @ApplicationQualifier context: Context): Retrofit {
        return Retrofit.Builder()
                .baseUrl(context.getString(R.string.baseUrl))
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }

    @Provides
    @Singleton
    fun providesDataSource(retrofit: Retrofit): DataSource = retrofit.create(DataSource::class.java)

}