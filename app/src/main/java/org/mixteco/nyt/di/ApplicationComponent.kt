package org.mixteco.nyt.di

import dagger.Component
import org.mixteco.nyt.di.subcomponent.main.MainComponent
import org.mixteco.nyt.di.subcomponent.main.MainModule
import javax.inject.Singleton

@Singleton
@Component(modules = [(ApplicationModule::class), (DataModule::class)])
interface ApplicationComponent {

    fun plus(module: MainModule) : MainComponent
}