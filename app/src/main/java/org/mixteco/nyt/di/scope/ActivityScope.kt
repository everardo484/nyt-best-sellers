package org.mixteco.nyt.di.scope

import javax.inject.Scope

@Scope
annotation class ActivityScope