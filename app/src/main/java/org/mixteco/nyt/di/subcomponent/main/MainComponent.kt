package org.mixteco.nyt.di.subcomponent.main

import dagger.Subcomponent
import org.mixteco.nyt.di.scope.ActivityScope
import org.mixteco.nyt.ui.MainActivity

@ActivityScope
@Subcomponent(modules = [(MainModule::class)])
interface MainComponent {

    fun injectTo(activity: MainActivity)
}