package org.mixteco.nyt.di

import android.content.Context
import android.support.v7.app.AppCompatActivity
import dagger.Module
import dagger.Provides
import org.mixteco.nyt.di.scope.ActivityScope

@Module
abstract class ActivityModule(val activity: AppCompatActivity) {
    @Provides
    @ActivityScope
    fun provideActivity(): AppCompatActivity = activity

    @Provides
    @ActivityScope
    fun provideActiviyContext(): Context = activity.baseContext
}