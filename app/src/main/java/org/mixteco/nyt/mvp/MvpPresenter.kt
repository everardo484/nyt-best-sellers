package org.mixteco.nyt.mvp

import android.support.annotation.UiThread

interface MvpPresenter<V : MvpView> {

    @UiThread
    fun attachView(view: V)

    @UiThread
    fun detachView()

    @UiThread
    fun onDestroy()
}
