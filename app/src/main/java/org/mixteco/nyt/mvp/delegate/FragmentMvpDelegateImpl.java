package org.mixteco.nyt.mvp.delegate;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.BackstackAccessor;
import android.support.v4.app.Fragment;
import android.view.View;

import org.mixteco.nyt.mvp.MvpPresenter;
import org.mixteco.nyt.mvp.MvpView;

import hugo.weaving.DebugLog;

public class FragmentMvpDelegateImpl<V extends MvpView, P extends MvpPresenter<V>> implements FragmentMvpDelegate<V, P> {
    private static final String TAG = FragmentMvpDelegateImpl.class.getSimpleName();
    private final MvpDelegateCallback<V, P> mDelegateCallback;
    private final Fragment mFragment;
    private final boolean keepPresenterOnBackstack;
    private boolean onViewCreatedCalled = false;

    public FragmentMvpDelegateImpl(@NonNull final Fragment fragment,
                                   @NonNull final MvpDelegateCallback<V, P> delegateCallback,
                                   final boolean keepPresenterOnBackstack) {
        mFragment = fragment;
        mDelegateCallback = delegateCallback;
        this.keepPresenterOnBackstack = keepPresenterOnBackstack;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle bundle) {
        getPresenter().attachView(getMvpView());
        onViewCreatedCalled = true;
    }

    @NonNull
    private Activity getActivity() {
        Activity activity = mFragment.getActivity();
        if (activity == null) {
            throw new NullPointerException("Activity returned by Fragment.getActivity() is null." +
                    " Fragment is " + mFragment);
        }

        return activity;
    }

    private P getPresenter() {
        return mDelegateCallback.getPresenter();
    }

    private V getMvpView() {
        V view = mDelegateCallback.getMvpView();
        if (view == null) {
            throw new NullPointerException("View returned from getMvpView() is null");
        }
        return view;
    }

    private boolean retainPresenterInstance() {
        if (getActivity().isFinishing()) {
            return false;
        }

        return (keepPresenterOnBackstack && BackstackAccessor.isFragmentOnBackStack(mFragment)) ||
                !mFragment.isRemoving();

    }

    @Override
    public void onDestroyView() {
        onViewCreatedCalled = false;
        getPresenter().detachView();
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onStart() {
        if (!onViewCreatedCalled) {
            throw new IllegalStateException("It seems that you are using "
                    + mDelegateCallback.getClass().getCanonicalName()
                    + " as headless (UI less) mFragment (because onViewCreated()" +
                    " has not been called or maybe delegation misses that part)." +
                    " Having a Presenter without a View (UI) doesn't make sense." +
                    " Simply use an usual mFragment instead of an MvpFragment if " +
                    "you want to use a UI less Fragment");
        }
    }

    @Override
    public void onStop() {

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

    }

    @Override
    public void onAttach(Activity activity) {

    }

    @Override
    public void onDetach() {

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

    @Override
    public void onCreate(Bundle bundle) {
    }

    @Override
    public void onDestroy() {
        onDestroy(retainPresenterInstance());
    }

    @DebugLog
    private void onDestroy(boolean retainPresenterInstance) {
        if (!retainPresenterInstance) {
            getPresenter().onDestroy();
        }
    }
}
