package org.mixteco.nyt.mvp.delegate;

import android.support.annotation.NonNull;

import org.mixteco.nyt.mvp.MvpPresenter;
import org.mixteco.nyt.mvp.MvpView;


public interface MvpDelegateCallback<V extends MvpView, P extends MvpPresenter<V>> {

    /**
     * @return presenter instance
     */
    @NonNull
    P getPresenter();

    /**
     * Gets the MvpView for the presenter
     *
     * @return The view associated with the presenter
     */
    V getMvpView();
}

