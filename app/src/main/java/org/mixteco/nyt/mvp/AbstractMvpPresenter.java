package org.mixteco.nyt.mvp;

import android.support.annotation.NonNull;
import android.support.annotation.UiThread;

import java.lang.ref.WeakReference;

import hugo.weaving.DebugLog;

public abstract class AbstractMvpPresenter<V extends MvpView> implements MvpPresenter<V> {
    private static final String TAG = AbstractMvpPresenter.class.getSimpleName();
    private WeakReference<V> viewRef;

    @DebugLog
    @UiThread
    @Override
    public void attachView(V view) {
        viewRef = new WeakReference<V>(view);
    }

    protected final void ifViewAttached(ViewAction<V> action) {
        final V view = viewRef == null ? null : viewRef.get();
        if (view != null) {
            action.run(view);
        }
    }

    @DebugLog
    @Override
    public void detachView() {
        if (viewRef != null) {
            viewRef.clear();
            viewRef = null;
        }
    }

    @DebugLog
    @Override
    public void onDestroy() { }

    public interface ViewAction<V> {
        void run(@NonNull V view);
    }
}
