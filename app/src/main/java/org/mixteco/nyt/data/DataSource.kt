package org.mixteco.nyt.data

import io.reactivex.Observable
import org.mixteco.nyt.model.BookResponse
import retrofit2.http.GET


interface DataSource {

    @GET("lists/overview.json?")
    fun fetchBooks() : Observable<BookResponse>

}