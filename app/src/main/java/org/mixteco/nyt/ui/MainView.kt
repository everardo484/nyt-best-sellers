package org.mixteco.nyt.ui

import org.mixteco.nyt.model.Book
import org.mixteco.nyt.mvp.MvpView


interface MainView : MvpView {
    fun showLoadingProgress()
    fun hideLoadingProgress()
    fun showActionMsg(msg: Int)
    fun onBooksLoadCompleted(books: MutableList<Book>)
}