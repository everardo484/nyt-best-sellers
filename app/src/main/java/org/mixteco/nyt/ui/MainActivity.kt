package org.mixteco.nyt.ui

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import org.mixteco.nyt.App
import org.mixteco.nyt.R
import org.mixteco.nyt.adapter.BooksAdapter
import org.mixteco.nyt.di.ApplicationComponent
import org.mixteco.nyt.di.subcomponent.main.MainModule
import org.mixteco.nyt.model.Book
import org.mixteco.nyt.mvp.MvpActivity

import javax.inject.Inject

class MainActivity : MvpActivity<MainView, MainPresenter>(), MainView {

    @Inject
    lateinit var mPresenter: MainPresenter

    @Inject
    lateinit var picasso: Picasso

    @Inject
    lateinit var mDataSet: MutableList<Book>

    override fun onCreate(savedInstanceState: Bundle?) {
        injectDependencies(App.graph)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)

        setupAdapter()

        fetchBooksIfNecessary()
    }

    fun fetchBooksIfNecessary() {
        if (mDataSet.isEmpty()) {
            presenter.fetchBooks()
        }
    }

    override fun getPresenter(): MainPresenter {
        return mPresenter
    }

    fun setupAdapter() {
        recyclerView.adapter = BooksAdapter(mDataSet, picasso)
    }

    override fun onBooksLoadCompleted(books: MutableList<Book>) {
        mDataSet.addAll(books)
        recyclerView.adapter.notifyDataSetChanged()
    }

    fun injectDependencies(applicationComponent: ApplicationComponent) {
        applicationComponent.plus(MainModule(this)).injectTo(this)
    }

    override fun showLoadingProgress() {
        loadingProgress.visibility = View.VISIBLE
    }

    override fun hideLoadingProgress() {
        loadingProgress.visibility = View.GONE
    }

    override fun showActionMsg(msg: Int) {
        Snackbar.make(mainView, msg, Snackbar.LENGTH_LONG)
                .setAction(R.string.snackbar_action, null)
                .show()

    }
}
