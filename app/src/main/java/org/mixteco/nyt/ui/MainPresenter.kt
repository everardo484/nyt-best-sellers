package org.mixteco.nyt.ui

import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.mixteco.nyt.R
import org.mixteco.nyt.data.DataSource
import org.mixteco.nyt.model.BookResponse
import org.mixteco.nyt.mvp.AbstractMvpPresenter

class MainPresenter(private val view: MainView, private val dataSource: DataSource) : AbstractMvpPresenter<MainView>() {

    fun fetchBooks() {
        dataSource.fetchBooks()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<BookResponse> {

                    override fun onSubscribe(d: Disposable) {
                        view.showLoadingProgress()
                    }

                    override fun onNext(response: BookResponse) {
                        for (list in response.results.bookList) {
                            view.onBooksLoadCompleted(list.books)
                        }
                    }

                    override fun onError(e: Throwable) {
                        view.hideLoadingProgress()
                        view.showActionMsg(R.string.book_fetch_error)
                    }

                    override fun onComplete() {
                        view.hideLoadingProgress()
                    }
                })
    }
}