package org.mixteco.nyt

import android.app.Application
import org.mixteco.nyt.di.ApplicationComponent
import org.mixteco.nyt.di.ApplicationModule
import org.mixteco.nyt.di.DaggerApplicationComponent

class App :Application() {

    companion object {
        lateinit var graph: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        initializeDagger()
    }

    private fun initializeDagger() {
        graph = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }
}