package org.mixteco.nyt.adapter

import android.content.res.Resources
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item.view.*
import org.mixteco.nyt.R
import org.mixteco.nyt.model.Book

class BooksAdapter(val books: MutableList<Book>, val picasso: Picasso) : RecyclerView.Adapter<BooksAdapter.ViewHolder>() {

    private lateinit var mResources: Resources

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        mResources = parent.resources

        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val book = books[position]
        picasso.load(book.bookImg).into(holder.itemView.movieImg)
        holder.itemView.title.text = book.title
        holder.itemView.author.text = String.format(mResources.getString(R.string.author, book.author))
        holder.itemView.description.text = book.description
        holder.itemView.weeksOnList.text = String.format(mResources.getString(R.string.weeks_on_list, book.weeksOnList))
    }

    override fun getItemCount(): Int {
        return books.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}