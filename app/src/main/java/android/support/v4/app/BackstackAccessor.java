package android.support.v4.app;

import android.support.annotation.NonNull;

import hugo.weaving.DebugLog;

public final class BackstackAccessor {

    private BackstackAccessor() {
        throw new IllegalStateException("Not instantiatable");
    }

    @DebugLog
    public static  boolean isFragmentOnBackStack(@NonNull Fragment fragment) {
        return fragment.isInBackStack();
    }
}
